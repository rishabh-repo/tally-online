<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name', 45)->index()->nullable();
			$table->string('company_name', 100)->index()->nullable();
			$table->string('mailing_name', 100)->index()->nullable();
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
			$table->string('address', 255)->index()->nullable();
			$table->string('city', 45)->index()->nullable();
			$table->string('state', 45)->index()->nullable();
			$table->string('pincode', 6)->index()->nullable();
			$table->string('country', 45)->index()->nullable();
			$table->string('mobile_no', 10)->index()->nullable();
			$table->date('financial_year_from')->index()->nullable();
			$table->date('book_from')->index()->nullable();
			$table->string('base_company', 10)->index()->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
