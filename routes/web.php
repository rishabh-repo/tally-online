<?php

use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\HomeController;
use \App\Http\Controllers\GroupController;
use \App\Http\Controllers\LedgerController;
use \App\Http\Controllers\Auth\LoginController;

/*
|
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/get_detail', function () { return view('welcome'); });
//Route::get('/', function () { return view('welcome'); });
Route::group(['Prefix' => '/'], function () {
	Route::group(['middleware' => 'auth'], function () {
		Route::get('/', [HomeController::class, 'index'])->name('index');

		Route::get('/masterGroup', [GroupController::class, 'loadGroup'])->name('group');
		Route::post('/submit', [GroupController::class, 'addGroup'])->name("submitGroup");

		Route::get('/masterLedger', [LedgerController::class, 'loadLedger'])->name('ledger');
	});
});


Auth::routes();
Route::get('/logout', [LoginController::class, 'logout'])->name('logout');
//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
