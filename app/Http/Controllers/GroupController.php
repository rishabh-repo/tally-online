<?php

namespace App\Http\Controllers;

use Auth;
use Redirect;
use App\Models\Group;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class GroupController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */


    public function loadGroup() {
        $group = Group::select()->orderBy('group_name', 'asc')->get();
        // view()->share('group', $group);
        return view('masters.group', compact('group'));
    }

    public function addGroup(Request $request) {
        $rules = [
            "group_name"    =>  'required|max:50',
            "group_alias"   =>  'required|max:50',
            "group_under"   =>  'required',
        ];
        $request->validate($rules);
        $list = Group::select()->where('group_name', $request->group_name)->count();
        
        if($list == 0) {
            $data = array(
                "group_name"    =>  $request->group_name,
                "group_alias"   =>  $request->group_alias,
                "group_under"   =>  $request->group_under,
                "created_at"    =>  date("Y-m-d H:i:s"),
                "updated_at"    =>  date("Y-m-d H:i:s")
            );
            Group::insert($data);
            // return Redirect::back()->with(['status'=>true, 'message'=>'Group Added Successfully..']);
            return ['status'=>'success', 'message'=>'Group Added Successfully..'];
        }else{
            return ['status'=>'failed', 'message'=>'Already Added in Group List <br> ('.$request->group_name.').'];
        }
    }


}
