<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/logout';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:50'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
			'mobile_no' => ['required', 'digits:10', 'unique:users'],
			'company_name' => ['required', 'string', 'max:100'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
			'address' => ['required', 'alpha_num', 'max:255'],
			'pincode' => ['required', 'digits:6'],
			'city' => ['required', 'string', 'max:40'],
			'state' => ['required', 'string', 'max:40'],
			'country' => ['required', 'string', 'max:40'],
			'financial_from' => ['required', 'date', 'date_format:Y-m-d'],
			'book_from' => ['required', 'date', 'date_format:Y-m-d'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => ucwords($data['name']),
            'email' => $data['email'],
			'mobile_no' => $data['mobile_no'],
			'company_name' => ucwords($data['company_name']),
			'mailing_name' => ucwords($data['company_name']),
            'password' => Hash::make($data['password']),
			'address' => ucwords($data['address']),
			'pincode' => $data['pincode'],
			'city' => ucwords($data['city']),
			'state' => ucwords($data['state']),
			'country' => ucwords($data['country']),
			'financial_year_from' => $data['financial_from'],
			'book_from' => $data['book_from'],
        ]);
    }
}
