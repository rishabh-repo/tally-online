<?php

namespace App\Http\Controllers;

use Auth;
use Redirect;
use App\Models\Group;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class LedgerController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function loadLedger() {
        $group = Group::select()->orderBy('group_name', 'asc')->get();
        return view('masters.ledger', compact('group'));
    }
}
