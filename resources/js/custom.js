$(document).ready(function(){
	$("#home_page_card").css("display", "block");
	$("#gateway_of_tally_card").css("display", "block");


	/* Gateway of Tally - Master Creation Search Filter Start */
	$("#creation_text").on("keyup", function() {
		var value = $(this).val().toLowerCase();
		$(".masters-body a").filter(function() {
  			$(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
		});
	});
	/** Gateway of Tally - Master Creation Search Filter End **/


	/* Group Creation Start */
	$("#group_name").on("keyup", function () {
		var group_name = $(this).val();
		$("#group_alias").val(group_name).addClass("text-capitalize");
	})
	/** Group Creation End **/


	$("#group_from").validate({
		ignore: ":hidden",
        rules: {
            group_name: {
                required: true,
            },
            group_alias: {
                required: true,
            },
            group_under: {
                required: true
            },
        },
        submitHandler: function (form) {
			$.ajax({
				type: "POST",
				url: $(form).attr("action"),
				data: $(form).serialize(),
				success: function (response) {
					if(response.status == 'success') {
						$("#group_from").trigger('reset');
						$("#group_list_section").load(location.href + " #group_list")
						toastr.success(response.message);
					}else{
						toastr.error(response.message);
					}
				}
			});
     	}
 	});







});


function getAnotherField (str, string) {
	if(string == 'statutory') {
		if(str == "Yes") {
			$("#statutory_field").css("display", "block");
		}else{
			$("#statutory_field").css("display", "none");
		}
	}
	if(string == 'tds'){
		if(str == 'Yes'){
			$("#teated_tds_expenses").css("display", "block");
		}else{
			$("#teated_tds_expenses").css("display", "none");
		}
	}
	if(string == "pan_it") {
		if(str == 'No'){
			$("#pan_stauts_div").css("display", "block");
		}else{
			$("#pan_stauts_div").css("display", "none");
		}
	}
	if(string == "pan_status") {
		if(str == ''){
			$("#pan_status_ref").css("display", "none");
		}else if(str == "Available"){
			$("#pan_status_ref").css("display", "none");
		}else{
			$("#pan_status_ref").css("display", "block");
		}
	}
	if(string == "treat_tds") {
		if(str == 'No'){
			$("#pay_nature_div").css("display", "none");
			$("#deductee_type_div").css("display", "block");
		}else{
			$("#deductee_type_div").css("display", "none");
			$("#pay_nature_div").css("display", "block");
		}
	}
}



function showMasterCreationList (str) {
	if(str == 'create'){
		$("#home_page_card").css("display", "none");
		$("#master_creation_card").css("display", "block");
	}
	if(str == 'group'){
		$("#ledger_creation").css("display", "none");
		$("#show_section_here").removeClass().addClass('col-md-6');
		$("#gateway_of_tally_card").css("display", "block");
		$("#group_creation").css("display", "block");
	}
	if(str == 'ledger'){
		$("#gateway_of_tally_card").css("display", "none");
		$("#group_creation").css("display", "none");
		$("#show_section_here").removeClass().addClass('col-md-9');
		$("#ledger_creation").css("display", "block");
	}
}

