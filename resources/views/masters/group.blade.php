@extends('layouts.header')
@section('content')
<div class="container-fluid">
	<div class="col-md-12">
	    <div class="row">
	    	<a href="{{ URL::route('index') }}">
				<button class="btn btn-secondary">
					<i class="fa fa-arrow-circle-left">&nbsp;</i> Back
				</button>
			</a>
			<section class="col-md-3" id="group_list_section">
				<div class="card" id="group_list">
					<div class="card-header text-center card-title">Group List</div>
					<div class="card-body group-list">
						<ul class="masters-body">
						@if(count($group) > 0)
                			@foreach($group AS $grp)
                			<li class="">{{ $grp->group_name }}</li>
                    		@endforeach
                		@endif
						</ul>
					</div>
				</div>
			</section>

			<section class="col-md-8">
				<div class="card">
					<div class="card-header text-center card-title">Group Creation</div>
					<div class="card-body">
						<form method="POST" id="group_from" action="{{ route('submitGroup') }}" autocomplete="off">
	                        @csrf
	                        <div class="form-group row">
	                            <div class="col-md-6">
                            		<label for="group_name">{{ __('Name :') }}</label>
	                                <input id="group_name" type="text" class="form-control @error('group_name') is-invalid @enderror" name="group_name" value="{{ old('group_name') }}" autocomplete="group_name" autofocus maxlength="50">
	                                @error('group_name')
	                                    <span class="invalid-feedback" role="alert">
	                                        <strong>{{ $message }}</strong>
	                                    </span>
	                                @enderror
	                            </div>
	                        
	                            <div class="col-md-6">
                            		<label for="group_alias">{{ __('(alias) :') }}</label>
	                                <input id="group_alias" type="text" class="form-control @error('group_alias') is-invalid @enderror" name="group_alias" value="{{ old('group_alias') }}" autocomplete="group_alias" autofocus readonly >
	                                @error('group_alias')
	                                    <span class="invalid-feedback" role="alert">
	                                        <strong>{{ $message }}</strong>
	                                    </span>
	                                @enderror
	                            </div>
	                        </div>

	                        <div class="form-group row">
	                            <label for="group_under" class="col-md-3 col-form-label text-md-right">{{ __('Under :') }}</label>
	                            <div class="col-md-8">
	                            	<select id="group_under" class="form-control @error('group_under') is-invalid @enderror" name="group_under" autocomplete="group_under" autofocus >
	                            		<option value="" selected disabled >Please Select</option>
	                            		<option value="Primary" >Primary</option>
                            		@if(count($group) > 0)
                            			@foreach($group AS $grp)
	                            		<option value="{{ $grp->group_name}}">{{ $grp->group_name }}</option>
	                            		@endforeach
                            		@endif
	                            	</select>
	                                @error('group_under')
	                                    <span class="invalid-feedback" role="alert">
	                                        <strong>{{ $message }}</strong>
	                                    </span>
	                                @enderror
	                            </div>
	                        </div>

	                        <div class="form-group row">
	                            <label for="group_behave_like" class="col-md-8 col-form-label text-md-right">{{ __('Group behaves like a sub-ledger :') }}</label>
	                            <div class="col-md-3">
	                            	<select id="group_behave_like" class="form-control" name="group_behave_like" autocomplete="group_behave_like" autofocus >
	                            		<option value="No" >No</option>
	                            		<option value="Yes" >Yes</option>
	                            	</select>
	                            </div>
	                        </div>

	                        <div class="form-group row">
	                            <label for="net_debit_credit_balance" class="col-md-8 col-form-label text-md-right">{{ __('Nett Debit/Credit Balances for Reporting :') }}</label>
	                            <div class="col-md-3">
	                            	<select id="net_debit_credit_balance" class="form-control" name="net_debit_credit_balance" autocomplete="net_debit_credit_balance" autofocus >
	                            		<option value="No" >No</option>
	                            		<option value="Yes" >Yes</option>
	                            	</select>
	                            </div>
	                        </div>

	                        <div class="form-group row">
	                            <label for="used_for_calculation" class="col-md-8 col-form-label text-md-right">{{ __('Used for calculation (for example: taxes, discounts) :') }}</label>
	                            <div class="col-md-3">
	                            	<select id="used_for_calculation" class="form-control" name="used_for_calculation" autocomplete="used_for_calculation" autofocus >
	                            		<option value="No" >No</option>
	                            		<option value="Yes" >Yes</option>
	                            	</select>
	                            </div>
	                        </div>

	                        <div class="form-group row">
	                            <label for="method_to_allocate" class="col-md-8 col-form-label text-md-right">{{ __('Method to allocate when used in purchase invoice :') }}</label>
	                            <div class="col-md-3">
	                            	<select id="method_to_allocate" class="form-control" name="method_to_allocate" autocomplete="method_to_allocate" autofocus >
	                            		<option value="Not Applicable" >Not Applicable</option>
	                            		<option value="Appropriate by Qty" >Appropriate by Qty</option>
	                            		<option value="Appropriate by Value" >Appropriate by Value</option>
	                            	</select>
	                            </div>
	                        </div>

	                        <div class="form-group row">
	                            <label for="set_alter_tdsDetail" class="col-md-8 col-form-label text-md-right">{{ __('Set/Alter TDS details :') }}</label>
	                            <div class="col-md-3">
	                            	<select id="set_alter_tdsDetail" class="form-control" name="set_alter_tdsDetail" autocomplete="set_alter_tdsDetail" autofocus >
	                            		<option value="No" >No</option>
	                            		<option value="Yes" >Yes</option>
	                            	</select>
	                            </div>
	                        </div>

	                        <div class="form-group row mb-0 mr-3">
	                            <div class="col-md-1 offset-md-11">
	                                <button type="submit" class="btn btn-secondary" id="save_group">
	                                    {{ __('Save') }}
	                                </button>
	                            </div>
	                        </div>
							
						</form>
					</div>
				</div>
			</section>
			<!-- // -->
	    </div>
	</div>
</div>

@endsection