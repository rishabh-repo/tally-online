@extends('layouts.header')
@section('content')
<div class="container-fluid">
	<div class="col-md-12">
	    <div class="row">
	    	<a href="{{ URL::route('index') }}">
				<button class="btn btn-secondary">
					<i class="fa fa-arrow-circle-left">&nbsp;</i> Back
				</button>
			</a>
			<section class="col-md-3" id="group_list_section">
				<div class="card" id="group_list">
					<div class="card-header text-center card-title">Ledger List</div>
					<div class="card-body group-list">
						<ul class="masters-body">
						
						</ul>
					</div>
				</div>
			</section>

			<section class="col-md-8">
				<div class="card">
					<div class="card-header text-center card-title">Ledger Creation</div>
					<div class="card-body">
						<form method="POST" action="#" autocomplete="off">
	                        @csrf
	                        <div class="form-group row">
	                            <div class="col-md-6">
	                            	<label for="group_name">{{ __('Name :') }}</label>
	                                <input id="group_name" type="text" class="form-control @error('group_name') is-invalid @enderror" name="group_name" value="{{ old('group_name') }}" autocomplete="group_name" autofocus maxlength="50">
	                                @error('group_name')
	                                    <span class="invalid-feedback" role="alert">
	                                        <strong>{{ $message }}</strong>
	                                    </span>
	                                @enderror
	                            </div>
	                        
	                            <div class="col-md-6">
	                            	<label for="group_alias">{{ __('(alias) :') }}</label>
	                                <input id="group_alias" type="text" class="form-control @error('group_alias') is-invalid @enderror" name="group_alias" value="{{ old('group_alias') }}" autocomplete="group_alias" autofocus readonly >
	                                @error('group_alias')
	                                    <span class="invalid-feedback" role="alert">
	                                        <strong>{{ $message }}</strong>
	                                    </span>
	                                @enderror
	                            </div>
	                        </div>

	                        <div class="row">
		                        <div class="col-md-6">
			                        <div class="form-group row">
			                            <label for="group_under" class="col-md-3 col-form-label text-md-right">{{ __('Under :') }}</label>
			                            <div class="col-md-9">
			                            	<select id="group_under" class="form-control @error('group_under') is-invalid @enderror" name="group_under" autocomplete="group_under" autofocus >
			                            		<option value="" selected disabled >Please Select</option>
			                            		@if(count($group) > 0)
			                            		@foreach($group as $gp)
			                            		<option value="{{ $gp->group_name}}">{{ $gp->group_name }}</option>
		                            			@endforeach
		                            			@endif
			                            	</select>
			                                @error('group_under')
			                                    <span class="invalid-feedback" role="alert">
			                                        <strong>{{ $message }}</strong>
			                                    </span>
			                                @enderror
			                            </div>
			                        </div>

			                        <span class="text-bold"><u>Statutory Details</u></span>
			                        <div class="form-group row">
			                            <label for="set_alter_statutory" class="col-md-8 col-form-label text-md-right">{{ __('Set / Alter Statutory details :') }}</label>
			                            <div class="col-md-4">
			                            	<select id="set_alter_statutory" class="form-control" name="set_alter_statutory" autocomplete="set_alter_statutory" autofocus onchange="getAnotherField(this.value, 'statutory')">
			                            		<option value="No" >No</option>
			                            		<option value="Yes" >Yes</option>
			                            	</select>
			                            </div>
			                        </div>

			                        <div id="statutory_field" style="display:none">
				                        <span class="text-bold"><u>TDS</u></span>
				                        <div class="form-group row" >
				                            <label for="tds_deductable" class="col-md-6 col-form-label text-md-right">{{ __('Is TDS Deductable :') }}</label>
				                            <div class="col-md-6">
				                            	<select id="tds_deductable" class="form-control" name="tds_deductable" autocomplete="tds_deductable" autofocus onchange="getAnotherField(this.value, 'tds')">
				                            		<option value="No" >No</option>
				                            		<option value="Yes" >Yes</option>
				                            	</select>
				                            </div>
				                        </div>

				                        <div id="teated_tds_expenses" style="display:none">
					                        <div class="form-group row">
					                            <label for="treat_tds_exp" class="col-md-6 col-form-label text-md-right">{{ __('Treat as TDS Expenses :') }}</label>
					                            <div class="col-md-6">
					                            	<select id="treat_tds_exp" class="form-control" name="treat_tds_exp" autocomplete="treat_tds_exp" autofocus onchange="getAnotherField(this.value, 'treat_tds')">
					                            		<option value="No" >No</option>
					                            		<option value="Yes" >Yes</option>
					                            	</select>
					                            </div>
					                        </div>
					                        <div id="deductee_type_div">
						                        <div class="form-group row">
						                            <label for="deductee_type" class="col-md-6 col-form-label text-md-right">{{ __('Deductee Type :') }}</label>
						                            <div class="col-md-6">
						                            	<select id="deductee_type" class="form-control" name="deductee_type" autocomplete="deductee_type" autofocus >
						                            		<option value="" selected >Unknown</option>
						                            		<option value="Artificial Juridical Person" >Artificial Juridical Person</option>
						                            		<option value="Association of Persons" >Association of Persons</option>
						                            		<option value="Body of Individuals">Body of Individuals</option>
						                            		<option value="Company - Non Resident">Company - Non Resident</option>
						                            		<option value="Company - Resident">Company - Resident</option>
						                            		<option value="Co-Operative Society - Non Resident">Co-Operative Society - Non Resident</option>
						                            		<option value="Co-Operative Society - Resident">Co-Operative Society - Resident</option>
						                            		<option value="Government">Government</option>
						                            		<option value="Individual/HUF - Non Resident">Individual/HUF - Non Resident</option>
						                            		<option value="Individual/HUF - Resident">Individual/HUF - Resident</option>
						                            		<option value="Local Authority">Local Authority</option>
						                            		<option value="Partnership Firm">Partnership Firm</option>
						                            	</select>
						                            </div>
						                        </div>
						                        <div class="form-group row">
						                            <label for="deductee_tds" class="col-md-8 col-form-label text-md-right">{{ __('Deductee TDS in Same Voucher :') }}</label>
						                            <div class="col-md-4">
						                            	<select id="deductee_tds" class="form-control" name="deductee_tds" autocomplete="deductee_tds" autofocus >
						                            		<option value="No" >No</option>
						                            		<option value="Yes" >Yes</option>
						                            	</select>
						                            </div>
						                        </div>
						                    </div>
						                    <div id="pay_nature_div" style="display:none">
						                        <div class="form-group row">
						                            <label for="nature_payment" class="col-md-6 col-form-label text-md-right">{{ __('Nature of Payment :') }}</label>
						                            <div class="col-md-6">
						                            	<select id="nature_payment" class="form-control" name="nature_payment" autocomplete="nature_payment" autofocus>
						                            		<option value="Undefined">Undefined</option>
						                            		<option value="Any">Any</option>
						                            	</select>
						                            </div>
						                        </div>
						                    </div>
					                        <div class="form-group row">
					                            <label for="pan_it_no" class="col-md-6 col-form-label text-md-right">{{ __('PAN/IT No. :') }}</label>
					                            <div class="col-md-6">
					                            	<select id="pan_it_no" class="form-control" name="pan_it_no" autocomplete="pan_it_no" autofocus onchange="getAnotherField(this.value, 'pan_it')">
					                            		<option value="Yes">Yes</option>
					                            		<option value="No">No</option>
					                            	</select>
					                            </div>
					                        </div>

					                        <div id="pan_stauts_div" style="display:none">
						                        <div class="form-group row">
						                            <label for="pan_stauts" class="col-md-6 col-form-label text-md-right">{{ __('PAN Status. :') }}</label>
						                            <div class="col-md-6">
						                            	<select id="pan_stauts" class="form-control" name="pan_stauts" autocomplete="pan_stauts" autofocus onchange="getAnotherField(this.value, 'pan_status')">
						                            		<option value="">Unknown</option>
						                            		<option value="Applied">Applied</option>
						                            	   	<option value="Available">Available</option>
						                            		<option value="Not Available">Not Available</option>
						                            		<option value="Not Required">Not Required</option>
						                            	</select>
						                            </div>
						                        </div>
						                    </div>

						                    <div id="pan_status_ref" style="display:none">
						                    	<div class="form-group row">
							                    	<label for="deductee_ref" class="col-md-4 col-form-label text-md-right">{{ __('Deductee Ref :') }}</label>
						                            <div class="col-md-8">
						                                <input id="deductee_ref" type="text" class="form-control @error('deductee_ref') is-invalid @enderror" name="deductee_ref" value="{{ old('deductee_ref') }}" autocomplete="deductee_ref" autofocus >
						                            </div>
						                        </div>
						                        <div class="form-group row">
							                    	<label for="tax_identify_no" class="col-md-6 col-form-label text-md-right">{{ __('Tax/Unique Identification Number :') }}</label>
						                            <div class="col-md-6">
						                                <input id="tax_identify_no" type="text" class="form-control @error('tax_identify_no') is-invalid @enderror" name="tax_identify_no" value="{{ old('tax_identify_no') }}" autocomplete="tax_identify_no" autofocus >
						                            </div>
						                        </div>
						                    </div>
					                    </div>
				                    </div>
			                    </div>



			                    <div class="col-md-6">
			                    	<span class="text-bold"><u>Mailing Details</u></span>
			                        <div class="form-group row">
			                            <label for="mailing_name" class="col-md-3 col-form-label text-md-right">{{ __('Name :') }}</label>
			                            <div class="col-md-8">
			                                <input id="mailing_name" type="text" class="form-control @error('mailing_name') is-invalid @enderror" name="mailing_name" value="{{ old('mailing_name') }}" autocomplete="mailing_name" autofocus maxlength="50">
			                                @error('mailing_name')
			                                    <span class="invalid-feedback" role="alert">
			                                        <strong>{{ $message }}</strong>
			                                    </span>
			                                @enderror
			                            </div>
			                        </div>
			                        <div class="form-group row">
			                            <label for="mailing_address" class="col-md-3 col-form-label text-md-right">{{ __('Address :') }}</label>
			                            <div class="col-md-8">
			                                <textarea id="mailing_address" class="form-control @error('mailing_address') is-invalid @enderror" name="mailing_address" autocomplete="mailing_address" autofocus rows="2">{{ old('mailing_address') }}</textarea>
			                                @error('mailing_address')
			                                    <span class="invalid-feedback" role="alert">
			                                        <strong>{{ $message }}</strong>
			                                    </span>
			                                @enderror
			                            </div>
			                        </div>
			                        <div class="form-group row">
			                            <label for="mailing_state" class="col-md-3 col-form-label text-md-right">{{ __('State :') }}</label>
			                            <div class="col-md-8">
			                                <input id="mailing_state" type="text" class="form-control @error('mailing_state') is-invalid @enderror" name="mailing_state" value="{{ old('mailing_state') }}" autocomplete="mailing_state" autofocus maxlength="50">
			                                @error('mailing_state')
			                                    <span class="invalid-feedback" role="alert">
			                                        <strong>{{ $message }}</strong>
			                                    </span>
			                                @enderror
			                            </div>
			                        </div>
			                        <div class="form-group row">
			                            <label for="mailing_country" class="col-md-3 col-form-label text-md-right">{{ __('Country :') }}</label>
			                            <div class="col-md-8">
			                                <input id="mailing_country" type="text" class="form-control @error('mailing_country') is-invalid @enderror" name="mailing_country" value="{{ old('mailing_country') }}" autocomplete="mailing_country" autofocus maxlength="50">
			                                @error('mailing_country')
			                                    <span class="invalid-feedback" role="alert">
			                                        <strong>{{ $message }}</strong>
			                                    </span>
			                                @enderror
			                            </div>
			                        </div>

			                        <span class="text-bold"><u>Banking Details</u></span>
			                        <div class="form-group row">
			                            <label for="provide_bank_detail" class="col-md-7 col-form-label text-md-right">{{ __('Provide Bank Details :') }}</label>
			                            <div class="col-md-4">
			                                <select id="provide_bank_detail" class="form-control" name="provide_bank_detail" autocomplete="provide_bank_detail" autofocus >
			                            		<option value="No" >No</option>
			                            		<option value="Yes" >Yes</option>
			                            	</select>
			                                @error('provide_bank_detail')
			                                    <span class="invalid-feedback" role="alert">
			                                        <strong>{{ $message }}</strong>
			                                    </span>
			                                @enderror
			                            </div>
			                        </div>

			                        <span class="text-bold"><u>Tax Registration Details</u></span>
			                        <div class="form-group row">
			                            <label for="pan_it_no" class="col-md-4 col-form-label text-md-right">{{ __('PAN/IT No. :') }}</label>
			                            <div class="col-md-7">
			                            	<input id="pan_it_no" type="text" class="form-control @error('pan_it_no') is-invalid @enderror" name="pan_it_no" value="{{ old('pan_it_no') }}" autocomplete="pan_it_no" autofocus maxlength="10">
			                                @error('pan_it_no')
			                                    <span class="invalid-feedback" role="alert">
			                                        <strong>{{ $message }}</strong>
			                                    </span>
			                                @enderror
			                            </div>
			                        </div>
			                        <div class="form-group row">
			                            <label for="registration_type" class="col-md-5 col-form-label text-md-right">{{ __('Registration Type :') }}</label>
			                            <div class="col-md-6">
			                                <select id="registration_type" class="form-control" name="registration_type" autocomplete="registration_type" autofocus >
			                            		<option value="" selected >Unknown</option>
			                            		<option value="Composition" >Composition</option>
			                            		<option value="Consumer" >Consumer</option>
			                            		<option value="Regular" >Regular</option>
			                            		<option value="Unregistered" >Unregistered</option>
			                            	</select>
			                                @error('registration_type')
			                                    <span class="invalid-feedback" role="alert">
			                                        <strong>{{ $message }}</strong>
			                                    </span>
			                                @enderror
			                            </div>
			                        </div>
			                        <div class="form-group row">
			                            <label for="gstin_uin" class="col-md-4 col-form-label text-md-right">{{ __('GSTIN / UIN :') }}</label>
			                            <div class="col-md-7">
			                                <input id="gstin_uin" type="text" class="form-control @error('gstin_uin') is-invalid @enderror" name="gstin_uin" value="{{ old('gstin_uin') }}" autocomplete="gstin_uin" autofocus maxlength="15">
			                                @error('gstin_uin')
			                                    <span class="invalid-feedback" role="alert">
			                                        <strong>{{ $message }}</strong>
			                                    </span>
			                                @enderror
			                            </div>
			                        </div>
			                        <div class="form-group row">
			                            <label for="set_alter_gst" class="col-md-7 col-form-label text-md-right">{{ __('Set/Alter GST details :') }}</label>
			                            <div class="col-md-4">
			                                <select id="set_alter_gst" class="form-control" name="set_alter_gst" autocomplete="set_alter_gst" autofocus >
			                            		<option value="No" >No</option>
			                            		<option value="Yes" >Yes</option>
			                            	</select>
			                                @error('set_alter_gst')
			                                    <span class="invalid-feedback" role="alert">
			                                        <strong>{{ $message }}</strong>
			                                    </span>
			                                @enderror
			                            </div>
			                        </div>
			                        <div class="form-group row">
			                            <label for="set_alter_service_tax" class="col-md-7 col-form-label text-md-right">{{ __('Set/Alter Service Tax Details :') }}</label>
			                            <div class="col-md-4">
			                                <select id="set_alter_service_tax" class="form-control" name="set_alter_service_tax" autocomplete="set_alter_service_tax" autofocus >
			                            		<option value="No" >No</option>
			                            		<option value="Yes" >Yes</option>
			                            	</select>
			                                @error('set_alter_service_tax')
			                                    <span class="invalid-feedback" role="alert">
			                                        <strong>{{ $message }}</strong>
			                                    </span>
			                                @enderror
			                            </div>
			                        </div>
			                        <div class="form-group row">
			                            <label for="set_alter_vat" class="col-md-7 col-form-label text-md-right">{{ __('Set/Alter VAT Details :') }}</label>
			                            <div class="col-md-4">
			                                <select id="set_alter_vat" class="form-control" name="set_alter_vat" autocomplete="set_alter_vat" autofocus >
			                            		<option value="No" >No</option>
			                            		<option value="Yes" >Yes</option>
			                            	</select>
			                                @error('set_alter_vat')
			                                    <span class="invalid-feedback" role="alert">
			                                        <strong>{{ $message }}</strong>
			                                    </span>
			                                @enderror
			                            </div>
			                        </div>
			                    </div>
			                </div>

	                        <div class="form-group row mb-0 mr-3">
	                            <div class="col-md-1 offset-md-11">
	                                <button type="submit" class="btn btn-primary">
	                                    {{ __('Save') }}
	                                </button>
	                            </div>
	                        </div>
							
						</form>
					</div>
				</div>
			</section>
			<!-- // -->
	    </div>
	</div>
</div>

@endsection