@extends('layouts.header')
@section('content')
<div class="container-fluid">
	<div class="col-md-12">
	    <div class="row">
	    	<section class="col-md-3" style="display: none" id="gateway_of_tally_card">
	            <div class="card">
	                <div class="card-body">
						<div class="card">
							<div class="card-header text-center card-title"> Gateway of Tally</div>
							<div class="card-body">
								<div class="col-md-12 gateway-card">
									<small class="text-primary">{{ __('MASTERS') }}</small>
									<div class="master-title ml-2">
										<a href="javascript:void(0)" onclick="showMasterCreationList('create')">{{ __('Create') }}</a>
									</div>
									<div class="master-title ml-2"><a href="javascript:void(0)">{{ __('Alter') }}</a></div>
									<div class="master-title ml-2"><a href="javascript:void(0)">{{ __('Chart of Accounts') }}</a></div>
									<div class="clearfix"></div>
									
									<small class="text-primary">{{ __('TRANSACTIONS') }}</small>
									<div class="master-title ml-2"><a href="javascript:void(0)">{{ __('Vouchers') }}</a></div>
									<div class="master-title ml-2"><a href="javascript:void(0)">{{ __('Day Book') }}</a></div>
									<div class="clearfix"></div>
									
									<small class="text-primary">{{ __('UTILITIES') }}</small>
									<div class="master-title ml-2"><a href="javascript:void(0)">{{ __('Banking') }}</a></div>
									<div class="clearfix"></div>
									
									<small class="text-primary">{{ __('REPORTS') }}</small>
									<div class="master-title ml-2"><a href="javascript:void(0)">{{ __('Balance Sheet') }}</a></div>
									<div class="master-title ml-2"><a href="javascript:void(0)">{{ __('Profit & Loss A/c') }}</a></div>
									<div class="master-title ml-2"><a href="javascript:void(0)">{{ __('Stock Summary') }}</a></div>
									<div class="master-title ml-2"><a href="javascript:void(0)">{{ __('Ratio Analysis') }}</a></div>
									<div class="clearfix"></div>
								</div>
							</div>
						</div>
	                </div>
	            </div>
	        </section>

	        <section class="col-md-3" style="display:none" id="master_creation_card">
				<div class="card">
					<div class="card-header master-header">					
						<h5 class="text-center text-bold mt-2 mb-0">Master Creation</h5>
					</div>
					<div class="card-header master-creation">						
						<!-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button> -->
						<form id="search_creation_form">
							<input type="text" name="creation_text" id="creation_text" class="form-control">
						</form>
						<h6 class="text-center text-bold mt-2 mb-0">List Of Masters</h6>
					</div>

					<div class="card-body body-popup">
						<h6 class="list-title text-bold mb-0 ml-2">{{ __('Accounting Masters') }}</h6>
						<div class="masters-body">
							<div class="ml-5 mb-0"><a href="{{URL::route('group')}}">{{ __('Group')}}</a></div>
							<div class="ml-5 mb-0"><a href="{{URL::route('ledger')}}">{{ __('Ledger') }}</a></div>
							<div class="ml-5 mb-0"><a href="#">{{ __('Cost Centre') }}</a></div>
							<div class="ml-5 mb-0"><a href="#">{{ __('Currency') }}</a></div>
							<div class="ml-5 mb-0"><a href="#">{{ __('Voucher Type') }}</a></div>
						</div>

						<h6 class="list-title text-bold mb-0 ml-2">{{ __('Inventory Masters') }}</h6>
						<div class="masters-body">
							<div class="ml-5 mb-0"><a href="#">{{ __('Stock Group') }}</a></div>
							<div class="ml-5 mb-0"><a href="#">{{ __('Stock Category') }}</a></div>
							<div class="ml-5 mb-0"><a href="#">{{ __('Stock Item') }}</a></div>
							<div class="ml-5 mb-0"><a href="#">{{ __('Unit') }}</a></div>
							<div class="ml-5 mb-0"><a href="#">{{ __('Location') }}</a></div>
							<div class="ml-5 mb-0"><a href="#">{{ __('Price Levels') }}</a></div>
							<div class="ml-5 mb-0"><a href="#">{{ __('Price List (Stock Group)') }}</a></div>
							<div class="ml-5 mb-0"><a href="#">{{ __('Price List (Stock Category)') }}</a></div>
						</div>

						<h6 class="list-title text-bold mb-0 ml-2">{{ __('Payroll Masters') }}</h6>
						<div class="masters-body">
							<div class="ml-5 mb-0"><a href="#">{{ __('Employee Group') }}</a></div>
							<div class="ml-5 mb-0"><a href="#">{{ __('Employee') }}</a></div>
							<div class="ml-5 mb-0"><a href="#">{{ __('Units (Work)') }}</a></div>
							<div class="ml-5 mb-0"><a href="#">{{ __('Attendance/Production Type') }}</a></div>
							<div class="ml-5 mb-0"><a href="#">{{ __('Pay Heads') }}</a></div>
							<div class="ml-5 mb-0"><a href="#">{{ __('Payroll Voucher Type') }}</a></div>
						</div>

						<h6 class="list-title text-bold mb-0 ml-2">{{ __('Statutory Masters') }}</h6>
						<div class="masters-body">
							<div class="ml-5 mb-0"><a href="#">{{ __('GST Classification') }}</a></div>
							<div class="ml-5 mb-0"><a href="#">{{ __('TDS Nature of Payments') }}</a></div>
							<div class="ml-5 mb-0"><a href="#">{{ __('TCS Nature of Goods') }}</a></div>
							<div class="ml-5 mb-0"><a href="#">{{ __('VAT Classification') }}</a></div>
							<div class="ml-5 mb-0"><a href="#">{{ __('Tax Units') }}</a></div>
							<div class="ml-5 mb-0"><a href="#">{{ __('Excise Classification') }}</a></div>
						</div>

						<h6 class="list-title text-bold mb-0 ml-2">{{ __('Statutory Details') }}</h6>
						<div class="masters-body">
							<div class="ml-5 mb-0"><a href="#">{{ __('GST Details') }}</a></div>
							<div class="ml-5 mb-0"><a href="#">{{ __('TDS Details') }}</a></div>
							<div class="ml-5 mb-0"><a href="#">{{ __('TCS Details') }}</a></div>
							<div class="ml-5 mb-0"><a href="#">{{ __('VAT Registration Details') }}</a></div>
							<div class="ml-5 mb-0"><a href="#">{{ __('Excise Registration Details') }}</a></div>
							<div class="ml-5 mb-0"><a href="#">{{ __('PAN/CIN Details') }}</a></div>
						</div>
					</div>
				</div>
			</section>

	        <section class="col-md-6">
	            <div class="card">
	                <div class="card-body">
						<div class="col-md-12">
							<small class="pull-left ml-3 text-primary">{{ __('CURRENT PERIOD') }}</small>
							<small class="pull-right mr-3 text-primary">{{ __('CURRENT DATE') }}</small>
							<div class="clearfix"></div>
							
							<p class="pull-left text-bold ml-3"> 1-April-{{ date('Y', strtotime('-1 year')) }} to 31-March-{{ date('Y') }}</p>
							<p class="pull-right text-bold mr-3"> 1-April-{{ date('Y', strtotime('-1 year')) }} to 31-March-{{ date('Y') }}</p>
							<div class="clearfix"></div>
							
							<div class="border-top-bottom">
								<small class="pull-left ml-3 text-primary">{{ __('NAME OF COMPANY') }}</small>
								<small class="pull-right mr-3 text-primary">{{ __('DATE OF LAST ENTRY') }}</small>
								<div class="clearfix"></div>
							</div>
							
							<div class="mt-4">
								<p class="pull-left text-bold ml-3">{{ Auth::user()->company_name }}</p>
								<p class="pull-right text-bold mr-3">{{ date('d M Y') }}</p>
							</div>
						</div>
	                </div>
	            </div>
	        </section>
	    </div>
	</div>
</div>

@endsection