<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name') }}</title>

    <!-- Scripts -->
    <!-- <script src="{{ asset('resources/js/app.js') }}" defer></script> -->
	<!-- <script src="{{ asset('resources/js/jquery.min.js') }}" defer></script> -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="{{ asset('resources/js/bootstrap.min.js') }}" defer></script>
    <script src="{{ asset('resources/js/custom.js') }}" defer></script>
    <!-- Validation Script -->
    <script src="{{ asset('resources/js/jquery.validate.min.js') }}" defer></script>
    <script src="{{ asset('resources/js/toastr.min.js') }}" defer></script>
    <!-- Notify Popup -->
    <script src="{{ asset('resources/js/notify.js') }}" defer></script>
    <script src="{{ asset('resources/js/notify.min.js') }}" defer></script>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <!-- Styles -->
    <link href="{{ asset('resources/css/custom.css') }}" rel="stylesheet">
	<link href="{{ asset('resources/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('resources/css/daterangepicker.css') }}" rel="stylesheet">
    <link href="{{ asset('resources/css/toastr.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm title-header">
            <div class="container-fluid">
                <span class="navbar-brand title-navbar">{{ Auth::user()->company_name }}</span>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>
                </div>
            </div>
            <a href="{{ route('logout')}}" onclick="return confirm('Are you sure..?')" class="navbar-brand title-navbar ">{{ __('Logout') }}</a>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>
